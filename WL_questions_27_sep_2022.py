# Q1
# Take 10 integers from keyboard using loop and print their average value on the screen.
# sum = 0
# i = 10
# while i > 0:
#     num = input("enter number")
#     sum = (int(sum) + int(num))
#     i = i-1
# print("average is", sum/10.0)

# practice
integers = 10
while integers > 0:
    user = int(input("enter number: "))
    integers = integers - 1
    sum = 0 + user
print("average is: ", sum/10)

# Q2
# Print the following patterns using loop :
# a.
# *
# **
# ***
# ****
# b.
#   *
#  ***
# *****
#  ***
#   *
# c.
# 1010101
#  10101
#   101
#    1
# A
i = 1
while i <= 4:
    print("*"*i)
    i = i + 1
# B
i = 1
j = 2
while i >= 1:
    a = " "*j+"*"*i+" "*j
    print(a)
    i = i + 2
    j = j - 1
    if i > 5:
        break
i = 3
j = 1
while i >= 1:
    a = " "*j+"*"*i+" "*j
    print(a)
    i = i - 2
    j = j + 1
# C
i = 7
j = 0
while i > 1:
    b = "  " * j + "10" * i + " " * j
    print(b)
    i = i - 2
    j = j + 1
    if i < 1:
        break

# Q3
# Print multiplication table of 24, 50 and 29 using loop.
i = 1
while i <= 10:
    print(24*i)
    i = i+1
j = 1
while j <= 10:
    print(50*j)
    j = j+1
k = 1
while k <= 10:
    print(29*k)
    k = k+1

# Q4
# Write an infinite loop.
# A inifinite loop never ends. Condition is always true.
while True:
    print("INFINITE")
