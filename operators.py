# # arithmatic operators
# +, _, *, **, /, //, %
print("20 + 3 is:", 20 + 3)
print("20 - 3 is :", 20 - 3)
print("20 * 2 is:", 20 * 3)
print("2 ** 3 is:", 2 ** 3)
print("20 / 3 is:", 20 / 3)
print("20 // 3 is:", 20 // 3)
print("20 % 3 is:", 20 % 3)

# assignment operators
a = 5
print("a", a)
# a += 3
# print(x)
# OR
# a = a + 3
# print("a2", a)
# b = 5
# b -= 3
# print("b", b)
# c = 5
# c *= 3
# print("c", c)
# d = 5
# d **= 3
# print("d", d)
# e = 5
# e /= 3
# print("e", 3)
# f = 5
# f //= 3
# print("f", f)
# g = 5
# g %= 3
# print("g", g)

# comparison operator
x = 5
print("a", x == 5)
print("b", x == 6)
print("c", x != 5)
print("d", x > 6)
print("e", x < 6)
print("f", x < 4)
print("g", x > 4)
print("h", x >= 6)
print("i", x <= 6)

# logical operators
# True, False, or, and
a = True
b = False
print(a and a)
print(a and b)
print(a or b)

# True and True = True
# True and False = False
# True or True = True
# True or False = True

# identity operators
#  is, is not
a = True
b = False
print(a is b)
print(a is not b)
print(5 is not 7)
print(5 is not 5)

# membership operators
# in, not in
a = True
b = False
list = [1, 2, 4, 8, 98, 100, 205]
print(100 in list)
print(100 not in list)
print(32 in list)
print(32 not in list)

