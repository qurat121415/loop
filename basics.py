a = 9
b = 8
c = sum((a, b))  # sum builtin_function
print(c)

def function1():  # user define function
    print("you are in function 1")
(function1())

def function1(a, b):  # fuction can also take input
    print("you are in func 1", a + b)
# function1(5, 5)
# if we wanted to store 10 value in a variable
v = function1(5, 5)

# taking average and storing it in a variable
def func(a, b):
    average = (a + b)/2
    print(average)
# func(5, 7)
# storing in a variable
v = func(5 , 7)
