# q1 tracking changes for merge branches
# Print all elements of a list using for loop.
list = ["quratulain", "arifain", "taybain"]
for items in list:
    print(items)

# q2
# Take inputs from user to make a list. Again take one input from user and
# search it in the list and delete that element, if found. Iterate over list using for loop.

list = []
user = int(input("no. of names: "))
for names in range(0, user):
    names = (input())
    list.append(names)
    print(list)
find = True
while find == True:
    user1 = (input("name you want to look for: "))
    for x in range(len(list)):
        if user1 == list[x]:
            print("yes, it is in the list")
            list.remove(user1)
            print(list)
            break
        elif x == (len(list)-1):
            print("no, that name is not in the list")
    user2 = input("would you like to search for another name? Y/N")
    if user2 == "y" or user2 == "yes":
        print("ok")
        find = True
    else:
        print("ok bye then")
        find = False
# AND
names = ["zul", "ari", "tay", "qur", "noor"]
find = True
while find == True:
    user = input("enter the name you are looking for: ")
    for x in range(len(names)):
        if user == (names[x]):
            print("yes that name is in the list")
            break
        elif x == (len(names)-1):
            print("that name is not in the list")
    user2 = input("would you like to search for another name? Y/N")
    if user2 == "y" or user2 == "yes":
        print("ok")
        find = True
    else:
        print("ok bye then")
        find = False

# remove elements from list within for loop
backpack = ["pizza slice", "button", "pizza slice", "fishing pole",
            "pizza slice", "nun chucks", "pizza slice", "sandwich"]
for items in backpack:
    print(items)
    if items == "pizza slice":
        backpack.remove(items)
# # error
for items in backpack[:]:
    if items == "pizza slice":
        backpack.remove(items)
print(backpack)
for items2 in backpack:
    print(items2)
# OR
new_backpack = []
for items in backpack:
    if items != "pizza slice":
        new_backpack.append(items)
print(new_backpack)

# Q3
# Print multiplication table of 14 from a list in which multiplication table of 12 is stored.
#
#
# Q4
# remove elements from list within for loop
backpack = ["pizza slice", "button", "pizza slice", "fishing pole",
            "pizza slice","nunchucks", "pizza slice", "sandwich"]
for items in backpack[:]:
    if items == "pizza slice":
        backpack.remove(items)
print(backpack)
for items in backpack:
    print(items)

# Q5
# creating new backpack
backpack = ["pizza slice", "button", "pizza slice", "fishing pole",
            "pizza slice", "nunchucks", "pizza slice", "sandwich"]
new_backpack = []
for items in backpack:
    if items != "pizza slice":
        new_backpack.append(items)
print(new_backpack)

# Q6
# You are given with a list of integer elements.
# Make a new list which will store square of elements of previous list.
list = [1, 2, 3, 4, 5, 6, 7, 8,]
new_list = []
for elements in list:
    new_list.append(elements**2)
    print(new_list)

# Q7
# Using range(1,101),make two list, one containing all even numbers and
# other containing all odd numbers.
even = []
odd = []
for x in range(1, 101):
    if x % 2 == 0:
        even.append(x)
    else:
        odd.append(x)
print(even)
print(odd)

# Q8
# From the two list obtained in previous question, make new lists,
# containing only numbers which are divisible by 4, 6, 8, 10, 3, 5, 7 and 9 in separate lists
new_list_4 = []
new_list_6 = []
new_list_8 = []
new_list_10 = []
new_list_3 = []
new_list_5 = []
new_list_7 = []
new_list_9 = []
for i in even:
    if i % 4 == 0:
        new_list_4.append(i)
    if i % 6 == 0:
        new_list_6.append(i)
    if i % 8 == 0:
        new_list_8.append(i)
    if i % 10 == 0:
        new_list_10.append(i)
for i in odd:
    if i % 3 == 0:
        new_list_3.append(i)
    if i % 5 == 0:
        new_list_5.append(i)
    if i % 7 == 0:
        new_list_7.append(i)
    if i % 9 == 0:
        new_list_9.append(i)
print(new_list_4)
print(new_list_6)
print(new_list_8)
print(new_list_10)
print(new_list_3)
print(new_list_5)
print(new_list_7)
print(new_list_9)

#  Q9
# From a list containing ints, strings and floats,
# make three lists to store them separately.
list = [1, 3, 22, 64, "1", "3", "22", "64", 1.0, 3.2, 22.4, 64.2]
integer = []
string = []
float = []
for x in list:
    if type(x) == int:
        integer.append(x)
        print(integer)
    elif type(x) == str:
        string.append(x)
        print(string)
    else:
        float.append(x)
        print(float)

# Q10
# Using range(1,101), make a list containing only prime numbers.

list = []
for num in range(1, 101):
    for x in range(2, num):
        if num % x == 0:
            break
    else:
        list.append(num)
        print(list)

# Q11
# Initialize a 2D list of 3*3 matrix. E.g.-
# 1	 2	3
# 4	 5	6
# 7	 8	9
# Check if the matrix is symmetric or not.
list = [[1, 2, 3], [2, 4, 5], [3, 5, 6]]
sym = True
for i in range(0, 3):
    for j in range(0, 3):
        if list[i][j] != list[j][i]:
            sym = False
print(sym)

# Q12
# Sorting refers to arranging data in a particular format.
# Sort a list of integers in ascending order ( without using built-in sorted function ).
# One of the algorithm is selection sort. Use below explanation of selection sort to do this.
# INITIAL LIST :
# 2	 3	1  45  15
# First iteration : Compare every element after first element with first element and
# if it is larger, then swap. In first iteration, 2 is larger than 1. So, swap it.
# 1	 3	2	45	15
# Second iteration : Compare every element after second element with second element and
# if it is larger, then swap. In second iteration, 3 is larger than 2. So, swap it.
# 1	 2	3	45	15
# Third iteration : Nothing will swap as 3 is smaller than every element after it.
# 1	 2	3	45	15
# Fourth iteration : Compare every element after fourth element with fourth element and
# if it is larger, then swap. In fourth iteration, 45 is larger than 15. So, swap it.
# 1	 2	3	15	45
a = [2, 3, 1, 45, 15]
for i in range(0, 5):
    for j in range(i+1, 5):
        if a[i] > a[j]:
            a[i], a[j] = a[j], a[i]
print(a)
