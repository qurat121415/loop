x = 0
while x < 1:
    print(x)
    x = x + 1
    # ans = 0 1 2 3 4 5 6 7 8 9 10
x = 0
while x < 10:
    print(x + 1)
    x = x + 1
    # ans = 1 2 3 4 5 6 7 8 9 10
x = 10
while x > 0:
    print(x)
    x = x - 1
#     # ans = 10 9 8 7 6 5 4 3 2 1
x = 10
while x > 0:
    print(x - 1)
    x = x - 1
#     # ans = 9 8 7 6 5 4 3 2 1 0

# 1. The number of guesses should be limited, i.e (5 or 9).
# 2. Print the number of guesses left.
# 3. Print the number of guesses he took to win the game.
# 4. The “Game Over” message should display if the number of guesses becomes equal to 0

# n = 18
no_of_guesses = 5
print("total no. of guesses that u have are 5")
while no_of_guesses > 0:
    user = int(input("guess a number: "))
    if user > 18:
        print("you entered a greater number")
        no_of_guesses = no_of_guesses - 1
        print("no of guesses left", no_of_guesses)
    elif user < 18:
        print("you entered a smaller number")
        no_of_guesses = no_of_guesses - 1
        print("no of guesses left", no_of_guesses)
    else:
        print("you won")
        print("number of guesses that user took to finish the game", no_of_guesses - 1)
        break
if no_of_guesses == 0:
    print("game over")
