# # 1 method
# # Take 10 integers from keyboard using loop and print their average value on the screen.
# sum = 0
# integers = 10
# while integers > 0:
#     user = int(input("enter number: "))
#     integers = integers - 1
#     sum = int(sum) + int(user)
# print("average is: ", sum/10)
#
# # 2 method
# sum = 0
# i = 10
# while i > 0:
#     num = input("enter number")
#     total = (int(sum) + int(num))
#     sum = total
#     i = i-1
# print("average is", total/10.0)

me = "Caleb"
# escape character example
print("\n")
x = "C\ta\tl\te\tb\n"
me = x
print(me)

# can also use single quotes
you = 'huma'
myself = "qurat-ul-ain" # reset to normal
# passing multiple arguments to print
print(myself, you)

# double quotes and single quotes work the same way with the exception of working with quotes inside.
# here are some examples:
single_quotes = 'She said "Hi"'
print(single_quotes)

double_quote = "She said \"Hi\""
print(double_quote)

single_quotes = 'I\'m learning!'
print(single_quotes)

double_quotes = "I'm learning!"
print(double_quotes)

print("she said \"hi\"")
print('she said \"hi\"')
print('she said \'hi\'')
print("she said \'hi\'")
print('it\'s beautiful')
print("it's beautiful")

# Notice that if you want to print \ you must put two
print("\\")

# you can also prefix with r which will make a raw string (ignoring escapes except same quote)
print(r'as raw as I\'ve ever seen. \/\/ () \/\/. \t')  # only \' is escaped

# ######### CONCATENATION ##########

# Use a + to concatenate
msg = myself + " + " + you
print(msg)

# Can use comma separated values in print. Automatically uses spaces between
print(myself, "+", you)

# You can automatically concatenate literals (actual values with quotes as opposed to variables)
# by putting them one after the other. This is ideal if you need to split a large string up
# onto multiple lines.
print("my " "name "
    "is " "Caleb")

# You can also use multiline string
print("""Name: Caleb
Age: 58""")

# skip newline using \ (without it, it would go down a line each line)
print("""\
Name: quratulain. \
Age: 22""")

"""You may see
them as multi-
line comments even
if they technically
are not. 
"""

print("""triple quotes string example 1""")
print('''triple quotes string example 2''')
print("""here's a string i'll split into
multiple
lines""")
print("""you can use 'single quotes" and "double quotes" inside triple quotes!""")

# ######### INDEXES ##########

# It's very common to grab particular characters within a string
# this is also common for collections when we get to em.
msg = "This is a very important message."
print(msg[5])

# indexing is zero based. This means 0 will get the very first character:
print(msg[0])

# This value is returned and can be assigned to a variable or used in an expression
first_letter = msg[0]
print(first_letter + "hat")

# You can also index from the right.
period = msg[32]  # from left
print(period)
period = msg[-1]  # from right
print(period)
# This may be obvious, but we do not use -0 to get the first element from the right as we would

# use 0 to get the first element from the left. (Side note) -0 actually is 0:
print(-0)  # (side note)
print(0 == -0)  # 0 == 0 (side note)

# ######### SLICING #########

# repeating this for ease of reading:
msg = "This is a very important message."

# We can pass two numbers in for the index to get a series of characters.
# left is included. Right is excluded.

# this will get 2 characters (from index 1 - 3 with 3 not included...we are left with index 1 and 2.
print(msg[1:3])  # hi

# You can also leave off first to start at beginning
# or leave off second to go to end
print(msg[:5])  # print index 0-4 (because 5 is excluded, remember)
print(msg[1:])  # from index 1 to end

# We can also use negatives. Here is how you get the last 8 characters:
print(msg[-8:])  # start 8 from right and go to end

# out of range index
# Grabbing an out of range index directly will cause an error.
# But incorrect ranges fail gracefully.
# print(msg[42]) #doesn't work
print(msg[42:43])  # works

# ######### IMMUTABILITY ##########

# Strings are immutable, meaning they can't change.
cant_change = "Java is my favorite!"

# generate new string from old:
# Kava is my favorite!
new = 'K' + cant_change[1:]
print(new)

# Python is my favorite!
fav_language = "Python" + cant_change[4:]
print(fav_language)

# Java is actually coffee
coffee = cant_change[:8] + "actually coffee"  # grab first 7 characters (index 8 not included)
print(coffee)

# operations that appear to change string actually replace:
# Java is actually coffee (contrary to popular belief).
coffee += " (contrary to popular belief)."
print(coffee)

# ######### GETTING STRING LENGTH ##########

# There is a function we can use....
# similar to how we invoke print to do something for us (output to console)
# we can invoke len to count for us:

print(len(coffee))

# last index is always len() - 1.
name = "quratulain"
print("index 4:", name[4:5])  # b
print("len(name)", len(name))  # length is 10

# ######### MORE STRING WORK ##########

# How to convert a number to a string
length = len(name)

print("Length is " + str(length))

# this works however sometimes you just need one combined string instead of components.
# when we use a comma, we are passing in data as separate arguments.
# fortunately, print knows how to handle it. Other times, we must pass in one string.
#  WARNING --> Commas automatically print a space.
print("length is ", length)
