# PRACTICE1
letters = ["a", "b", "c"]
for x in letters:
    print(x)

# p2
# print each fruit in a fruit list.

fruits = ["apple", "orange", "pineapple", "grapes", "mango"]
for fruit in fruits:
    print(fruit)

# p3
# looping through string
for x in "watermelon":
    print(x)

# p4
# break statement

fruits = ["apple", "orange", "pineapple", "grapes", "mango"]
for x in fruits:
    print(x)
    if x == "pineapple":
        break
print("or")
for x in fruits:
    if x == "pineapple":
        break
    print(x)

# p7
# continue statement

fruits = ["apple", "orange", "pineapple", "grapes", "mango"]
# do not print pineapple
for x in fruits:
    # print(x) WRONG STEP
    if x == "pineapple":
        continue
    print(x)

# p8
# range function

for x in range(6):
    print(x)
print("or")
for x in range(1, 7):
    print(x)
print("or") # printing tables
for x in range(2, 20, 2):
    print(x)
for x in range(9, 90, 9):
    print(x)

# p9
# else in for loop
# print all members from 0 t0 5 and print a message when the loop has ended

for x in range(6):
    print(x)
else:
    print("done")

# p10
# nested loop

adj = ["red", "yellow", "green"]
fruits = ["tomato", "banana", "watermelon"]
for x in adj:
    for y in fruits:
        print(x, y)

# P11
# checking if a number is prime number or not
n = 22
for x in range(2, n):
    if n % x == 0:
        print("not a prime number")
        break
if n % x != 0:
    print("prime number")
